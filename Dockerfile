############################################################
# Dockerfile to run OpenESB Standalone
# Based on Ubuntu Image
############################################################

# DOCKER-VERSION 0.7.1
FROM      ubuntu:14.04
MAINTAINER David BRASSELY <brasseld@gmail.com>

ENV OE_VERSION	3.0.3

# make sure the package repository is up to date
RUN echo "deb http://archive.ubuntu.com/ubuntu trusty main universe" > /etc/apt/sources.list
RUN apt-get -y update

# install python-software-properties (so you can do add-apt-repository)
RUN DEBIAN_FRONTEND=noninteractive apt-get install -y -q python-software-properties software-properties-common

# install SSH server so we can connect multiple times to the container
RUN apt-get -y install openssh-server && mkdir /var/run/sshd

# install oracle java from PPA
RUN add-apt-repository ppa:webupd8team/java -y
RUN apt-get update
RUN echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | /usr/bin/debconf-set-selections
RUN apt-get -y install oracle-java8-installer && apt-get clean

# Set oracle java as the default java
RUN update-java-alternatives -s java-8-oracle
RUN echo "export JAVA_HOME=/usr/lib/jvm/java-8-oracle" >> ~/.bashrc

# install utilities
RUN apt-get -y install vim git sudo zip bzip2 fontconfig curl

# configure the "openesb" and "root" users
RUN echo 'root:openesb' |chpasswd
RUN groupadd openesb && useradd openesb -s /bin/bash -m -g openesb -G openesb && adduser openesb sudo
RUN echo 'openesb:openesb' |chpasswd

# Install OpenESB
RUN mkdir /openesb && \
    cd /openesb && \
    wget http://repo1.maven.org/maven2/net/open-esb/runtime/standalone/openesb-standalone-packaging/$OE_VERSION/openesb-standalone-packaging-$OE_VERSION.zip && \
    unzip openesb-standalone-packaging-$OE_VERSION.zip && \
    rm -f openesb-standalone-packaging-$OE_VERSION.zip

# Define mountable directories.
VOLUME ["/data"]

# ADD openesb.sh /openesb.sh

# Define working directory.
WORKDIR /data

RUN chmod +x /openesb/bin/*.sh

CMD ["/usr/sbin/sshd", "-D"]
CMD ["/openesb/bin/openesb.sh"]

# Expose OpenESB Web Console, SSHD port, and run SSHD
EXPOSE 4848
EXPOSE 22
